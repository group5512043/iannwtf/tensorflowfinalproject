# Project: Estimating Photovoltaic Installation Potential for Rooftops from Aerial Images using CNNs and Computer Vision

This repository contains the code for the project titled "Estimating Photovoltaic Installation Potential for Rooftops from Aerial Images using CNNs and Computer Vision" by Robin Daubert & Simon Rebers.

## Description
The project aims to estimate the potential for installing photovoltaic panels on rooftops using aerial images and convolutional neural networks (CNNs) combined with computer vision techniques.

## Main Files
The main files in this project are:

1. **train_model.py**: This file contains code to start the training process for the CNN model.
2. **load_model.py**: This file contains code to start the testing process on a pre-trained model. The model to be tested must be chosen within this file.
