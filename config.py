# Variables for configuration

# Model specifications
EPOCHS = 100
INPUT_SIZE = 128
BATCH_SIZE = 32

# Number of training images used during training
NUM_IMAGES = 5000

# Number of validation images used during training
VAL_NUM = 1000

# Paths where models are stored
PATH_MODEL_SAVES = "saves/model_saves"

# Paths of the dataset
TRAIN_INPUT = "D:/kaggle/train/image"
TRAIN_LABEL = "D:/kaggle/train/label"
TEST_INPUT = "D:/kaggle/test/image"
TEST_LABEL = "D:/kaggle/test/label"

# Paths for the split images of the dataset
TRAIN_SPLIT_INPUT = "D:/kaggle/split_data/input"
TRAIN_SPLIT_LABEL = "D:/kaggle/split_data/label"
TEST_SPLIT_INPUT = "D:/kaggle/split_data/validation/input"
TEST_SPLIT_LABEL = "D:/kaggle/split_data/validation/label"

# Paths for high resolution split images
HIGH_RES_INPUT = "D:/kaggle/split_data/high_res/input"
HIGH_RES_LABEL = "D:/kaggle/split_data/high_res/label"

# Path for sample test data in the project
TEST_IMAGES = "data/test_images"
