import os

if not os.path.exists('saves'):
    os.mkdir('saves')

if not os.path.exists('saves/extracted_buildings'):
    os.mkdir('saves/extracted_buildings')

if not os.path.exists('saves/logs'):
    os.mkdir('saves/logs')

if not os.path.exists('saves/model_saves'):
    os.mkdir('saves/model_saves')

if not os.path.exists('saves/training_history'):
    os.mkdir('saves/training_history')
