import cv2
import numpy as np
from PIL import Image, ImageFilter
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from src.visualization import plot_final_image

# This file splits the roof in its parts using clustering and cv2.find_contours


def generate_colors(num_colors):
    # Get a colormap that consist of 20 colors
    cmap = plt.cm.get_cmap('tab20', num_colors)
    colors = []
    for i in range(num_colors):
        rgba = cmap(i)
        # Convert RGBA values to RGB values from 0 to 255
        rgb = tuple(int(rgba[j] * 255) for j in range(3))
        colors.append(rgb)
    return colors


def edge_detection(roof):

    gray_roof = cv2.cvtColor(roof, cv2.COLOR_BGR2GRAY)

    # Threshold the image to obtain a binary image
    _, binary_image = cv2.threshold(gray_roof, 0, 255, cv2.THRESH_BINARY)

    # Find contours in the binary image
    contours, _ = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    image_list = []
    cut_outs = 0

    # Iterate through contours
    for i, contour in enumerate(contours):
        # Create an empty mask
        mask = np.zeros_like(gray_roof)

        # Draw selected contours
        cv2.drawContours(mask, contours[i:i+1], -1, (255, 255, 255), thickness=cv2.FILLED)

        # Calculate area size
        area_pixels = np.count_nonzero(mask)
        area_size = round(area_pixels * 0.005625, 2)

        minimal_area = 3.0  # m^2

        if area_size > minimal_area:
            rgb_mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
            image_list.append((rgb_mask, area_size))

        else:
            cut_outs += area_pixels

    return image_list, cut_outs


def split_roof(roof_cutout, background_image, new_dir, num_buildings, input_size):
    image = cv2.imread(roof_cutout)

    # Calculate the roof size
    _, roof_size_img = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY)
    total_roof_size = np.count_nonzero(roof_size_img)
    print(f"Total roof size: {total_roof_size}")

    # Reshape the image to a 2D array of pixels
    pixels = image.reshape(-1, 3)

    # This list contains results on a variation of clusters
    cluster_list = []

    # A cluster range between 2-6 clusters have proven to create the best results
    for num_cluster in range(2, 6):
        # This list stores all areas that are extracted on the current amount of cluster
        areas_list = []

        # Cutouts stores the pixels that were filtered out as noise
        cutouts = 0

        # Run the clustering algorithm
        kmeans = KMeans(n_clusters=num_cluster)
        kmeans.fit(pixels)

        # Iterate through the created clusters
        for label_index in range(1, num_cluster):

            # Create a binary mask for the current label
            binary_mask = (kmeans.labels_ == label_index).reshape((input_size, input_size))
            binary_mask = binary_mask.astype(np.uint8)

            # Combine the binary mask with the input image
            roof = cv2.bitwise_and(image, image, mask=binary_mask)

            # Run edge detection on the created image to separate areas and filter out noise
            new_areas, cutout = edge_detection(roof)

            # Extend the area list with all new areas
            areas_list.extend(new_areas)

            cutouts += cutout

        # Get a color palette for visual distinction
        colors = generate_colors(20)

        mask = np.zeros_like((input_size, input_size, 3))

        # Create a list with sizes for the legend of the final image plot
        size_list = []

        for i, (area, area_size) in enumerate(areas_list):

            # Color the area
            color_mask = np.all(area != [0, 0, 0], axis=-1)
            try:
                area[color_mask] = colors[i]
            except:
                print("Roof was split in too many parts")

            # Add colored areas to the same image
            mask = cv2.add(mask, area)

            size_list.append(area_size)

        mask = Image.fromarray(mask)

        # Apply median filter
        try:
            mask = mask.filter(ImageFilter.ModeFilter(size=8))
        except:
            print("Edge smoothing of Clustered areas failed")

        # Convert the filtered mask back to numpy array
        mask = np.array(mask)

        cluster_list.append((mask, num_cluster, len(areas_list), cutouts, size_list))

    # Function for best fit: choose the maximum amount of areas for an area percentage over 90
    best_fit = max(cluster_list, key=lambda x: (x[2] * int(((np.count_nonzero(x[0]) / total_roof_size) * 100) > 90.0)))

    area_percentage = ((np.count_nonzero(best_fit[0]) / total_roof_size) * 100)
    print(f"The prediction covers {area_percentage}% of the predicted building area")

    # Create the final image by blending the best fit mask onto the input image
    mask, _, _, _, size_list = best_fit
    background = background_image
    overlay = Image.fromarray(mask, "RGB")

    background = background.convert("RGBA")
    overlay = overlay.convert("RGBA")

    final_image = Image.blend(background, overlay, 0.5)

    # Prepare data for the plot
    cmap = plt.cm.get_cmap('tab20', 20)
    legend_colors = [cmap(i) for i in range(20)]

    combined_list = sorted(zip(size_list, legend_colors), key=lambda x: x[0], reverse=True)
    size_list, legend_colors = zip(*combined_list)

    plot_final_image(final_image, size_list, legend_colors, new_dir, num_buildings)
