import glob
import datetime
import os
import random
import cv2
import numpy as np
from PIL import Image, ImageFilter
import keras
from config import PATH_MODEL_SAVES, HIGH_RES_INPUT, TEST_IMAGES
from building_separation import building_separation
from src.visualization import plot_input_and_prediction

# This file loads available models and starts a test process with visualizations on the chosen model
# The data is stored in saves/extracted_buildings

# Number of test images that are used
input_number = 5


def make_square(image, width, height):
    image_size = min(width, height)
    input_subarray = image[:image_size, :image_size, :]
    img = np.uint8(input_subarray)
    print(f"Cut image to size: {img.shape}")
    pil_img = Image.fromarray(img, 'RGB')

    return pil_img, image_size




print(os.listdir(f"../{PATH_MODEL_SAVES}"))

model_saves = sorted(os.listdir(f"../{PATH_MODEL_SAVES}"))

print(f"{len(model_saves)} model(s) found to load:")
print("------------------------------------------------------------------------")

for i, file_name in enumerate(model_saves):
    print(f"[{i + 1}] {file_name}")

print("------------------------------------------------------------------------")

model_index = int(input("enter number to load the model: "))

model_name = model_saves[model_index - 1]
loaded_model = keras.models.load_model(f"../{PATH_MODEL_SAVES}/{model_name}")

# Create new dir to save the results
current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
dir_list = os.listdir('../saves/extracted_buildings')
new_dir = f"../saves/extracted_buildings/extraction{len(dir_list)}_{current_time}({model_name})"
os.mkdir(new_dir)

test_images = sorted(glob.glob(f'../{TEST_IMAGES}/*.png'))
random.shuffle(test_images)
test_images = test_images[:input_number]

print(f"Testing {input_number} out of {len(test_images)} images:")

for i, test_image in enumerate(test_images):
    # Create new dir for the extraction
    num_extractions = len(os.listdir(new_dir)) + 1
    image_dir = f"{new_dir}/image{num_extractions}"
    os.mkdir(image_dir)

    img = Image.open(test_image)

    # Test if the input images is a square and if not cut it to a square
    test_shape = np.array(img)
    width, height, _ = test_shape.shape
    print(f" Shape of image: {test_shape.shape}")

    if width != height:
        cut_image, input_size = make_square(test_shape, width, height)
    else:
        cut_image = img
        input_size = width
    print(f"Cut image shape: {cut_image}")

    # Downscale the resolution to 128x128 to match the model input size
    img = cut_image.resize((128, 128), Image.Resampling.LANCZOS)

    try:
        # If image has 4 color channels:
        red, green, blue, _ = img.split()
        img = Image.merge("RGB", (red, green, blue))
    except:
        print("Image has 3 color channels")

    batched_img = np.array(img)

    batched_img = np.expand_dims(batched_img, axis=0)

    # Predict the output
    prediction = loaded_model.predict(batched_img, batch_size=16)

    # Convert to 0 and 1 using the threshold 0.5
    prediction = (prediction > 0.5).astype(np.uint8)

    # Multiply by 255 to create a higher contrast
    prediction_image = prediction[0].reshape((128, 128)) * 255

    # Plot the input images next to the prediction images
    plot_input_and_prediction(img, prediction_image, image_dir)

    mask = prediction[0, :, :, 0]
    mask = Image.fromarray(mask, 'L')

    # Up-scale the prediction
    mask = mask.resize((input_size, input_size), Image.Resampling.LANCZOS)

    mask = np.uint8(mask)

    # Find contours and draw them on a new mask
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    mask_image = np.zeros_like(mask)
    cv2.drawContours(mask_image, contours, -1, (255, 255, 255), thickness=cv2.FILLED)

    # Use median filter to smooth the up-scaled mask
    mask_image = Image.fromarray(mask_image, 'L')
    smooth_edges = mask_image.filter(ImageFilter.ModeFilter(size=13))
    smooth_edges = np.uint8(smooth_edges)

    building_separation(cut_image, smooth_edges, image_dir, cut_image, input_size)
