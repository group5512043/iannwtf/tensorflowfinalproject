import glob
import cv2
import numpy as np
from split_roof import split_roof

# This file extracts different buildings of the model prediction


def building_separation(input_path, gray_mask, new_dir, background_image, input_size):

    rgb_input = np.array(input_path)
    rgb_input = cv2.cvtColor(rgb_input, cv2.COLOR_BGR2RGB)
    cv2.imwrite(f'{new_dir}/input.png', rgb_input)

    # Threshold the image to obtain a binary image
    _, binary_image = cv2.threshold(gray_mask, 127, 255, cv2.THRESH_BINARY)

    # Find contours in the binary image
    contours, _ = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    print(f"{len(contours)} Contours found")

    # Iterate through contours
    for i, contour in enumerate(contours):
        # Create an empty mask
        mask = np.zeros_like(gray_mask)

        # Draw selected contours
        cv2.drawContours(mask, contours[i:i+1], -1, (255, 255, 255), thickness=cv2.FILLED)

        # Apply the binary mask to the RGB input image
        result = cv2.bitwise_and(rgb_input, rgb_input, mask=mask)

        # Create a greyscale mask to estimate the area
        gray_result = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY)

        # The airs dataset has a spatial resolution of 0.075m
        # Each pixel equals 0.005625 m^2

        # Estimate the area
        area_pixels = np.count_nonzero(gray_result)
        area_size = round(area_pixels * 0.005625, 2)  # 1pixel has an area of 0.140625m^2

        # Select a minimal area to cut out noise
        minimal_area = 5.0  # (m^2)

        if area_size > minimal_area:
            num_buildings = len(glob.glob(f'{new_dir}/building*).png')) + 1
            file_name = f'{new_dir}/building{num_buildings}({area_size}m2).png'
            cv2.imwrite(file_name, result)

            split_roof(file_name, background_image, new_dir, num_buildings, input_size)
