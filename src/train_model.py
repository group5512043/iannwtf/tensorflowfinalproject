import glob
import datetime
import os
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Input
from config import EPOCHS, INPUT_SIZE, NUM_IMAGES, PATH_MODEL_SAVES, BATCH_SIZE, TEST_SPLIT_INPUT, \
    TEST_SPLIT_LABEL, TRAIN_SPLIT_INPUT, TRAIN_SPLIT_LABEL, VAL_NUM
from visualization import plot_model_history, check_data
from models import u_net_plusplus, u_net

# This file starts the training process on the model that is imported from models.py

print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))


def load_data():
    train_img = sorted(glob.glob(f'{TRAIN_SPLIT_INPUT}/*.png'))  # adjust filetype
    train_label = sorted(glob.glob(f'{TRAIN_SPLIT_LABEL}/*.png'))

    print(f"train images: {len(train_img)}, train labels: {len(train_label)}")

    train_label = train_label[:NUM_IMAGES]
    train_img = train_img[:NUM_IMAGES]

    ds = tf.data.Dataset.from_tensor_slices((train_img, train_label))
    ds = ds.map(load_image_and_mask, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    return ds


def load_validation_data():
    val_img = sorted(glob.glob(f'{TEST_SPLIT_INPUT}/*.png'))
    val_label = sorted(glob.glob(f'{TEST_SPLIT_LABEL}/*.png'))

    print(f"validation images: {len(val_img)}, validation labels: {len(val_label)}")

    val_label = val_label[:VAL_NUM]
    val_img = val_img[:VAL_NUM]

    ds = tf.data.Dataset.from_tensor_slices((val_img, val_label))
    ds = ds.map(load_image_and_mask, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    return ds


def load_image_and_mask(img, label):
    image = tf.io.read_file(img)
    image = tf.image.decode_image(image, channels=3)

    mask = tf.io.read_file(label)
    mask = tf.image.decode_image(mask, channels=1)
    mask = mask / 255
    return image, mask


x_in = Input(shape=(INPUT_SIZE, INPUT_SIZE, 3))

# x_out = u_net(x_in)
x_out = u_net_plusplus(x_in)

model = Model(inputs=x_in, outputs=x_out)

model.compile(loss='mse', optimizer='adam', metrics=['binary_accuracy'])

model.summary()

# Define where to save the log
files = os.listdir(f"../{PATH_MODEL_SAVES}")
model_name = f"model{len(files) + 1}_{EPOCHS}epochs_{NUM_IMAGES}images_{BATCH_SIZE}bs"
current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
train_log_path = f"../saves/logs/{current_time}({model_name})"

# Log writer for training metrics
train_summary_writer = tf.keras.callbacks.TensorBoard(log_dir=train_log_path, histogram_freq=1)

print("Preparing data...\n")
dataset = load_data()
val_data = load_validation_data()
dataset = dataset.shuffle(buffer_size=1000)

# Plot 5 samples to check the data
# sample_list = dataset.take(1)
# check_data(sample_list)
# val_sample_list = val_data.take(1)
# check_data(val_sample_list)

# Batch the data
dataset = dataset.batch(BATCH_SIZE).prefetch(tf.data.experimental.AUTOTUNE)
val_data = val_data.batch(BATCH_SIZE).prefetch(tf.data.experimental.AUTOTUNE)

# Train the model
print(f"Training Model ({EPOCHS} epochs, {NUM_IMAGES} images, {BATCH_SIZE} batch_size):")
print("-----------------------------------------------------------------------------------------------------------")

history = model.fit(dataset, validation_data=val_data, epochs=EPOCHS, verbose=1, callbacks=[train_summary_writer])

# Save the model
model.save(f"../{PATH_MODEL_SAVES}/{model_name}.keras")

print("Model training finished")

# Plot model loss over training and validation data
plot_model_history(history)

