from datetime import datetime
from matplotlib import pyplot as plt
from config import EPOCHS, NUM_IMAGES, BATCH_SIZE

# This file contains visualization functions


def check_data(sample_list):
    for image, mask in sample_list:
        print(f"image shape: {image.shape}, mask shape: {mask.shape}")

        plt.figure(figsize=(10, 5))

        # Plot image
        plt.subplot(1, 2, 1)
        plt.imshow(image)
        plt.title('Image')
        plt.axis('off')

        # Plot mask
        plt.subplot(1, 2, 2)
        plt.imshow(mask, cmap="gray")
        plt.title('Mask')
        plt.axis('off')

        plt.show()


def plot_model_history(history):
    current_time = datetime.now().strftime("%Y%m%d-%H%M%S")

    plt.plot(history.history['loss'], color='blue')
    plt.plot(history.history['val_loss'], linestyle='--', color='blue')
    plt.plot(history.history['binary_accuracy'], color='orange')
    plt.plot(history.history['val_binary_accuracy'], linestyle='--', color='orange')

    plt.title('Model Loss and Accuracy')
    plt.ylabel('Value')
    plt.xlabel('Epoch')
    plt.legend(['Train Loss', 'Validation Loss', 'Train Accuracy', 'Validation Accuracy'], loc='upper left')

    plt.savefig(f'../saves/training_history/{current_time}_{EPOCHS}epochs_{NUM_IMAGES}images_{BATCH_SIZE}bz.png')

    plt.show()


def plot_final_image(final_image, new_size_list, legend_colors, new_dir, num_buildings):
    fig, ax = plt.subplots()

    ax.imshow(final_image)

    # Plot empty bars to create legend
    for i, (label, color) in enumerate(zip(new_size_list, legend_colors)):
        ax.bar([0], [0], color=color, label=label)

    # Hide axes
    ax.axis('off')

    # Add legend
    ax.legend(loc='upper left', bbox_to_anchor=(1, 1))
    ax.set_title(f'Split roof of building {num_buildings}\n\nLegend: area sizes in m^2')  # Add description
    ax.axis('off')

    plt.savefig(f"{new_dir}/building{num_buildings}_final_prediction")
    plt.show()


def plot_input_and_prediction(input_image, prediction_image, image_dir):
    fig, axes = plt.subplots(1, 2, figsize=(10, 5))

    # Plot input image
    axes[0].imshow(input_image)
    axes[0].set_title('Input Image')
    axes[0].axis('off')

    # Plot prediction
    axes[1].imshow(prediction_image, cmap='gray')
    axes[1].set_title('Prediction')
    axes[1].axis('off')

    plt.savefig(f'{image_dir}/comparison.png')
    plt.show()
