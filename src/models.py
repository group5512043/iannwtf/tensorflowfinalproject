from tensorflow.keras.layers import Conv2D, MaxPooling2D, Conv2DTranspose, Concatenate, Dropout, BatchNormalization, Activation

# This file contains the implementations of a UNet and a UNet++


def unet_convolution_block(x, num_filter):
    x = Conv2D(num_filter, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(num_filter, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    return x


def unet_transposed_convolution_block(x, num_filter):
    x = Conv2DTranspose(num_filter, (3, 3), activation='relu', padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2DTranspose(num_filter, (3, 3), strides=(2, 2), activation='relu', padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    return x


def convolution_block(x, num_filter):
    x = Conv2D(num_filter, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(num_filter, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    return x


def transposed_convolution_layer(x, num_filter):
    x = Conv2DTranspose(num_filter, (3, 3), strides=(2, 2), activation='relu', padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    return x


def upscale(x, num_filter):
    x = Conv2DTranspose(num_filter, (3, 3), strides=(2, 2), activation='relu', padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    return x


def u_net(x_in):
    # Encoder
    x_skip_1 = unet_convolution_block(x_in, 32)

    x = MaxPooling2D((2, 2))(x_skip_1)

    x_skip_2 = unet_convolution_block(x, 32)

    x = MaxPooling2D((2, 2))(x_skip_2)

    x_skip_3 = unet_convolution_block(x, 64)

    x = MaxPooling2D((2, 2))(x_skip_3)

    x = unet_convolution_block(x, 64)

    # Decoder
    x = unet_transposed_convolution_block(x, 64)

    x = Concatenate()([x, x_skip_3])

    x = unet_transposed_convolution_block(x, 64)

    x = Concatenate()([x, x_skip_2])

    x = unet_transposed_convolution_block(x, 32)

    x = Concatenate()([x, x_skip_1])

    # Output layers
    x = Conv2D(32, (1, 1), activation='relu', padding='same')(x)
    x = Conv2D(32, (1, 1), activation='relu', padding='same')(x)
    x_out = Conv2D(1, (1, 1), activation='sigmoid', padding='same')(x)
    # Sigmoid to approximate binary images

    return x_out


def u_net_plusplus(x_in):

    num_filters = [16, 32, 64, 128]

    # X(0,0)
    x_0_0 = convolution_block(x_in, num_filters[0])
    x_0_0_down = MaxPooling2D((2, 2))(x_0_0)

    # X(1,0)
    x_1_0 = convolution_block(x_0_0_down, num_filters[1])
    x_1_0_up = transposed_convolution_layer(x_1_0, num_filters[0])
    x_1_0_down = MaxPooling2D((2, 2))(x_1_0)

    # X(0,1)
    x_0_1_in = Concatenate()([x_0_0, x_1_0_up])
    x_0_1 = convolution_block(x_0_1_in, num_filters[0])

    # X(2,0)
    x_2_0 = convolution_block(x_1_0_down, num_filters[2])
    x_2_0_up = transposed_convolution_layer(x_2_0, num_filters[1])
    x_2_0_down = MaxPooling2D((2, 2))(x_2_0)

    # X(1,1)
    x_1_1_in = Concatenate()([x_1_0, x_2_0_up])
    x_1_1 = convolution_block(x_1_1_in, num_filters[1])
    x_1_1_up = transposed_convolution_layer(x_1_1, num_filters[0])

    # X(0,2)
    x_0_2_in = Concatenate()([x_0_0, x_0_1, x_1_1_up])
    x_0_2 = convolution_block(x_0_2_in, num_filters[0])

    # X(3,0)
    x_3_0 = convolution_block(x_2_0_down, num_filters[3])
    x_3_0_up = transposed_convolution_layer(x_3_0, num_filters[2])

    # X(2,1)
    x_2_1_in = Concatenate()([x_2_0, x_3_0_up])
    x_2_1 = convolution_block(x_2_1_in, num_filters[2])
    x_2_1_up = transposed_convolution_layer(x_2_1, num_filters[1])

    # X(1,2)
    x_1_2_in = Concatenate()([x_1_0, x_1_1, x_2_1_up])
    x_1_2 = convolution_block(x_1_2_in, num_filters[1])
    x_1_2_up = transposed_convolution_layer(x_1_2, num_filters[0])

    # X(0,3)
    x_0_3_in = Concatenate()([x_0_0, x_0_1, x_0_2, x_1_2_up])
    x_0_3 = convolution_block(x_0_3_in, num_filters[0])

    # Output layers
    x = Conv2D(32, (1, 1), activation='relu', padding='same')(x_0_3)
    x = Conv2D(32, (1, 1), activation='relu', padding='same')(x)
    x_out = Conv2D(1, (1, 1), activation='sigmoid', padding='same')(x)
    # Sigmoid to approximate binary images

    return x_out
