import glob
import random
import re

import numpy as np
from PIL import Image

from config import TEST_INPUT, TEST_LABEL, TEST_SPLIT_INPUT, TEST_SPLIT_LABEL, HIGH_RES_INPUT, HIGH_RES_LABEL, \
    TRAIN_INPUT, TRAIN_LABEL, TRAIN_SPLIT_INPUT, \
    TRAIN_SPLIT_LABEL, INPUT_SIZE

Image.MAX_IMAGE_PIXELS = None

# This script splits images of the airs dataset

# Splitting specifications:
# Limit of images that are converted
split_limit = 50

# Specify if the respective label needs to be stored
with_label = True

# Specify if the images are downscaled or not
# Training images use 128x128 resolutions, test images for the building extraction and splitting use 640x640 resolutions
high_res = True

# Configure correct paths that are defined in config.py
input_path = TRAIN_INPUT
split_input_path = TRAIN_SPLIT_INPUT

label_path = TRAIN_LABEL
split_label_path = TRAIN_SPLIT_LABEL

if high_res:
    image_size = INPUT_SIZE * 5
else:
    image_size = INPUT_SIZE


def split_array(input_array, label_array, subarray_size, image_number):

    # Calculate the number of subarrays in each dimension
    num_subarrays_x = input_array.shape[0] // subarray_size
    num_subarrays_y = input_array.shape[1] // subarray_size

    # Iterate over each subarray and append it to the list
    for i in range(num_subarrays_x):
        for j in range(num_subarrays_y):
            input_subarray = input_array[i*subarray_size:(i+1)*subarray_size, j*subarray_size:(j+1)*subarray_size, :]
            img = np.uint8(input_subarray)
            pil_img = Image.fromarray(img, 'RGB')

            label_subarray = label_array[i*subarray_size:(i+1)*subarray_size, j*subarray_size:(j+1)*subarray_size, :]
            label = np.uint8(label_subarray)
            label = np.squeeze(label)
            pil_label = Image.fromarray(label, 'L')

            rand_num = random.random()
            pixel_num_img = np.count_nonzero(img)
            pixel_num_label = np.count_nonzero(label)

            if pixel_num_img > 0:
                if pixel_num_label > 0 or rand_num < 0.00:

                    # Path for the split input images
                    pil_img.save(f"{split_input_path}/input{image_number}_{(i*num_subarrays_x+j)}.png")

                    if with_label:
                        # Path for the split label images
                        pil_label.save(f"{split_label_path}/label{image_number}_{(i*num_subarrays_x+j)}.png")


def extract_number(filename):
    return int(re.search(r'\d+', filename).group())


if __name__ == "__main__":
    images = sorted(glob.glob(f'{input_path}/*.tif'), key=lambda x: extract_number(x))
    labels = sorted(glob.glob(f'{label_path}/*vis.tif'), key=lambda x: extract_number(x))

    print(f"Images: {len(images)}, Limit: {split_limit}")

    for i, (img, label) in enumerate(zip(images, labels)):
        if i == split_limit:
            break
        print(f"splitting image {i + 1}...")
        img = Image.open(img)
        try:
            # If image has 4 color channels:
            red, green, blue, _ = img.split()
            img = Image.merge("RGB", (red, green, blue))
        except:
            print("image has 3 color channels")

        # Downscale the resolution
        if not high_res:
            img = img.resize((2000, 2000), Image.Resampling.LANCZOS)
        np_img = np.array(img)

        label = Image.open(label)

        # Downscale the resolution
        if not high_res:
            label = label.resize((2000, 2000), Image.Resampling.LANCZOS)
            np_mask = np.array(label).reshape(2000, 2000, 1)
        else:
            np_mask = np.array(label).reshape(10000, 10000, 1)

        # Split the image into a given size
        split_array(np_img, np_mask, image_size, i)
